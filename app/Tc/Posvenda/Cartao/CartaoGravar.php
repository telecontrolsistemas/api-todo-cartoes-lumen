<?php

namespace App\Tc\Posvenda\Cartao;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response;
use App\Models\CartaoServico;
use App\Tc\Posvenda\Cartao\ValidarObrigatorios as oCamposObrigatorios;


class CartaoGravar implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {
        $oCartaoServico = new CartaoServico();

        $save = $data;
	unset($save['data']);
	unset($save['results']);
	unset($save['pages']);
	unset($save['perPage']);
	unset($save['current']);
	unset($save['type']);
	unset($save['id']);
	unset($save['originalCreatedAt']);
	unset($save['originalTransactionCode']);
	unset($save['daysToExpire']);
	$save['fabrica'] = $identifier;

	if (!empty($data['originalTransactionCode'])) {
		$save['cancelamento'] = 't';
	}

        foreach ($save as $key => $val) {
            $oCartaoServico->$key = $val;
        }

        $oCartaoServico->save();

	$data["id"] = $oCartaoServico->numero_cartao;
	$data["request_method"] = "post";
	$data["type"] = "cartao_todos";
	$data["links"] = ["self" => $_SERVER['REQUEST_URI']."/".$oCartaoServico->numero_cartao];

        return $next->process($identifier, $data);
    }
}

?>
